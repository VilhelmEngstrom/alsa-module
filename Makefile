CC ?= gcc
OUTFILE = alsa_module

INSTALL_PATH = ~/.bin/status

SRC = $(wildcard src/*.c)
OBJ := $(addsuffix .o, $(basename $(SRC)))

INC = -I src/

CFLAGS := $(CFLAGS) -std=c11 -Wall -Wextra -pedantic -Wshadow -Wunknown-pragmas $(INC)
LDFLAGS = -lasound -lm

$(OUTFILE): $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)

.PHONY: clean run release install

clean:
	rm -f $(OBJ) $(OUTFILE)

run: $(OUTFILE)
	./$(OUTFILE)

release: CFLAGS := -O3 $(CFLAGS)
release: $(OUTFILE)

install: release
	cp $(OUTFILE) $(INSTALL_PATH)
