#include "volume_bar.h"

#include <math.h>
#include <stdio.h>

#define EPSILON 1e-5

void print_bar(double vol, unsigned muted) {
    int i;
    char bar[BAR_WIDTH + 2];
    bar[BAR_WIDTH + 1] = '\0';
    if(muted || fabs(vol) < EPSILON) {
        printf("%s sound muted", MUTED_ICON);
        return;
    }
    for(i = 0; i < BAR_WIDTH + 1; i++) {
        bar[i] = BAR_FILL;
    }
    bar[(int)(vol * (double)BAR_WIDTH)] = BAR_INDICATOR;

    printf("%s %s", UNMUTED_ICON, bar);
}
