# ALSA Module

Custom ALSA module for [Polybar](https://github.com/polybar/polybar). The default ALSA module doesn't play particularly well with how I have one of my machines set up so I created my own (much simpler) version.

[![Build Status](https://gitlab.com/vilhelmengstrom/alsa-module/badges/master/pipeline.svg)](https://gitlab.com/vilhelmengstrom/alsa-module/commits/master)

#### Options
- `--card` - specify the PCM interface. Default value is `default`.
- `--ctrl` - specify the control. A list of available options can be gotten by running `amixer controls`. Default value is `SoftMaster`.
- `--sctrl` - specify the scontrol. `amixer scontrols` lists available options. Default value is `IEC958`.
- `--sctrl_idx` - specify the scontrol index.  Default value is `1`.

#### Dependencies
The default icons are from [Font Awesome 5](https://fontawesome.com). They can be changed in `src/volume_bar.h`.
